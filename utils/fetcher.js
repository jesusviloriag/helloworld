export function getPopular(language, page, pageSize, HTMLParser) {
    console.log("https://stackoverflow.com/questions/tagged/" + language + "?tab=votes&page=" + page +"&pagesize=" + pageSize);
    return fetch("https://stackoverflow.com/questions/tagged/" + language + "?tab=votes&page=" + page +"&pagesize=" + pageSize)
    .then((res) => {
        return res.text();
    })
    .then((data) => {
        var itens = [];
        var HTMLParser = require('fast-html-parser');
        var doc = HTMLParser.parse( data );
        
        var respuestasHtml = doc.querySelectorAll(".question-hyperlink");
        console.log(respuestasHtml);
        for(var i = 0;  i < respuestasHtml.length; i++) {
            var respuesta = respuestasHtml[i];
            var testo = respuesta.childNodes[0].rawText.replace("&ldquo;","\"").replace("&rdquo;","\"").replace("&gt;",">").replace("&lt;","<").replace("&#39;","<").replace("                    ","");
            //if(testo.includes("How")) {
                var iten = {
                    key: (i + 1) + "",
                    titulo: testo,
                    link: respuesta.attributes.href,
                }
                itens.push(iten);
            //}
        }
        
        return itens;
    });
}

export function getBuscar(query, language, page, pageSize, HTMLParser) {
    console.log("https://stackoverflow.com/search??tab=votes&q=[" + language + "]" + query + "&page=" + page +"&pagesize=" + pageSize);
    return fetch("https://stackoverflow.com/search??tab=votes&q=[" + language + "]" + query + "&page=" + page +"&pagesize=" + pageSize)
    .then((res) => {
        return res.text();
    })
    .then((data) => {
        var itens = [];
        var HTMLParser = require('fast-html-parser');
        var doc = HTMLParser.parse( data );
        
        var respuestasHtml = doc.querySelectorAll(".question-hyperlink");
        console.log(respuestasHtml);
        for(var i = 0;  i < respuestasHtml.length; i++) {
            var respuesta = respuestasHtml[i];
            var testo = respuesta.childNodes[0].rawText.replace("&ldquo;","\"").replace("&rdquo;","\"").replace("&gt;",">").replace("&lt;","<").replace("&#39;","<").replace("                    ","");
            //if(testo.includes("How")) {
                var iten = {
                    key: (i + 1) + "",
                    titulo: testo,
                    link: respuesta.attributes.href,
                }
                itens.push(iten);
            //}
        }
        
        return itens;
    });
}

export function getAnswer(url) {
    var link = "";
    if(!url.includes("https://")) {
        link = "https://stackoverflow.com" + url;
    }
    console.log(link);

    if(link) {
        return fetch(link)
            .then((res) => {
                return res.text();
            })
            .then((data) => {
                var resultado = "<span>";
                var datos = data.split("<code>");
                var accepted = false;
                for(i = 0; i < datos.length; i++) {
                    if(datos[i].includes("answer accepted-answer")) {
                        console.log("ACCEPTED");
                        accepted = true;
                    } else if (datos[i].includes("class=\"answer\"")) {
                        console.log("NOT ACCEPTED");
                        accepted = false;
                    }
                    if(i%2) {
                        var dato = datos[i]
                        codes = dato.split("</code>");
                        for (var j = 0; j < codes.length; j++ ) {
                            if(!j%2) {
                                var code = codes[j];
                                if(accepted && code.includes(" ")){
                                    console.log(code);
                                    resultado += code + " ";
                                }
                            }
                        }
                    }
                }
                return resultado + "</span>";
            })
            .catch((error) => {
                console.error(error);
            });
    } else return null;
  }