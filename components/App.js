import React, { Component } from 'react';
import { FlatList, TextInput, StyleSheet, Text, View, TouchableOpacity , Platform} from 'react-native';

import Codigo from './Codigo';

import { 
  AdMobBanner, 
  AdMobInterstitial
} from 'react-native-admob'

export default class App extends Component {

  constructor(props) {
    super(props);

    var showAds = true;

    var idquestionAd2 = '';
      if (Platform.OS == "ios")
        idquestionAd2 = 'ca-app-pub-2466516236387107/3755796111';
      else
        idquestionAd2 = 'ca-app-pub-2466516236387107/3090657107';

    this.state = {
      mensaje: 'Hello, World!',
      numero: 25,
      placeHolder: 'Search language',
      text: '',
      paginaWeb: 'https://google.com',
      listaDeCodigosFuente: [],
      refreshing: false,
      idquestionAd2: idquestionAd2,
      showAds: showAds
    }

    /*if(showAds) {
      setTimeout(function() {
        var idquestionAd = '';
        if (Platform.OS == "ios")
          idquestionAd = 'ca-app-pub-2466516236387107/5056451250';
        else
          idquestionAd = 'ca-app-pub-2466516236387107/6709326159';

        AdMobInterstitial.setAdUnitID(idquestionAd);
        AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
        AdMobInterstitial.requestAd().then(() => AdMobInterstitial.showAd());
      }, 10000)     
    }*/

  }

  componentDidMount() {
    this.getCodigos();
  }

  getCodigos() {
    this.setState({ refreshing: true });
    fetch("https://www.guaire.net/assets/helloworldapp/codigos.json")
      .then((response) => response.json())
      .then((responseJson) => {
        var codigos = [];
        if(responseJson.codigos) {
          for (var i = 0; i < responseJson.codigos.length; i++) {
            var codigo = responseJson.codigos[i];
            codigo.navigator = this.props.navigator;
            codigos.push(codigo);
          }
        }

        this.setState({ refreshing: false });
        this.setState({ listaDeCodigosFuente: codigos });
      })
      .catch((error) => {
        console.error(error);
      });
  }

  getBanner() {
    if(this.state.showAds) {
      return(
        <AdMobBanner
            adSize="bigBanner"
            adUnitID={this.state.idquestionAd2}
            testDevices={[AdMobBanner.simulatorId]}
            onAdFailedToLoad={error => console.error(error)}
          />
      )
    } else {
      return null;
    }
    
  }

  render() {
    return (

      <View style={styles.container}>

        <FlatList
          data={this.state.listaDeCodigosFuente}
          renderItem={({ item }) => <Codigo nombre={item.nombre} foto={item.foto} ejemplo={item.ejemplo} navigator={item.navigator}/>}
          onRefresh={this.getCodigos.bind(this)}
          refreshing={this.state.refreshing}
        />


        {this.getBanner()}

        

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 33,
    textAlign: 'center',
    marginTop: 35,
    marginBottom: 10,
    fontWeight: 'bold',
    color: '#0044aa'
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
