import React, { Component } from 'react';
import { Share, Clipboard, ScrollView, WebView, Image, Dimensions, TextInput, Platform, StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { getAnswer } from '../utils/fetcher';

import HTMLView from 'react-native-htmlview';

function renderNode(node, index, siblings, parent, defaultRenderer) {
  if (node.name == 'span') {
      return (
        <Text 
          key={index} 
          style={{
            fontSize: 14,
            backgroundColor: '#272822',
            fontFamily: 'Courier',
            padding: 15,
            flex: 1,
            color: 'white'
          }}>
          {defaultRenderer(node.children, parent)}
        </Text>
      )
    }
}

export default class Respuesta extends Component {

  constructor(props) {
    super(props);

    this.state = {
      ejemplo: "",
      link: props.link
    }
  }

  componentDidMount() {
    this.getRespuesta();
  }

  getRespuesta() {
    this.setState({ refreshing: true });
    var _this = this;
    _this.setState({showRespuesta: false})
    try {
      getAnswer(this.state.link)
        .then(function(response){
          console.log(response);
          if(response && response !== "<span></span>") {
            _this.setState({ejemplo: response, refreshing: false, mostrar: true, showRespuesta: true})
            _this.forceUpdate();
          }
        })
        .catch((error) => {
          console.error(error);
        });
    } catch(ex) {
      console.log(ex);
    }
    
  }

  share() {
    Share.share({
      message: this.state.ejemplo.replace("<span>","").replace("</span>","").replace("&ldquo;","\"").replace("&rdquo;","\"").replace("&gt;",">").replace("&lt;","<").replace("&#39;","<").replace("                    ","").replace("&quot;","\"").replace("&quot;","\""),
      title: this.props.nombre
    });
  }

  copiar() {
    Clipboard.setString(this.state.ejemplo.replace("<span>","").replace("</span>","").replace("&ldquo;","\"").replace("&rdquo;","\"").replace("&gt;",">").replace("&lt;","<").replace("&#39;","<").replace("                    ","").replace("&quot;","\"").replace("&quot;","\""))
  }

  showRespuesta() {
    if(this.state.showRespuesta) {
      return (
        <HTMLView
          value={this.state.ejemplo}
          stylesheet={styles}
          renderNode={renderNode}
        />  
      )
    } else {
      return null;
    }
  }

  mostrar() {
    if(this.state.mostrar) {
      return (
        <View style={styles.background}>
          <View style={styles.containerSided}>
            <Text style={styles.nombre}>{this.props.nombre}</Text>
            <TouchableOpacity style={{
              width: 35,
              height: 35,
              padding: 15,
              borderRadius: 10,
              backgroundColor: '#0044aa',
              alignItems: 'center',
              justifyContent: 'center',
              marginRight: 10
            }}
              onPress={this.copiar.bind(this)}
            >
              <Image
                style={{ width: 25, height: 25 }}
                resizeMode={"contain"}
                source={require('../assets/copy.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity style={{
              width: 35,
              height: 35,
              padding: 15,
              borderRadius: 10,
              backgroundColor: '#0044aa',
              alignItems: 'center',
              justifyContent: 'center'
            }}
              onPress={this.share.bind(this)}
            >
              <Image
                style={{ width: 25, height: 25 }}
                resizeMode={"contain"}
                source={require('../assets/share.png')}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.containerSided}>
            <ScrollView horizontal={true} style={styles.scrollStyle}>
              {this.showRespuesta()}
            </ScrollView>
          </View>

        </View>
      )
    } else {
      return null;
    }
  }

  
  render() {
    return (
      this.mostrar()
    );
  }
}

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#95e8ff',
    width: Dimensions.get('window').width,
    padding: 25,
    borderBottomWidth: 2,
    borderBottomColor: 'white'
  },
  containerSided: {
    flexDirection: 'row'
  },
  nombre: {
    fontSize: 22,
    textAlign: 'left',
    margin: 10,
    color: '#272822',
    fontWeight: 'bold',
    flex: 1
  },
  ejemplo: {
    fontSize: 14,
    backgroundColor: '#272822',
    fontFamily: 'Courier',
    padding: 15,
    flex: 1,
    color: 'white'
  },
  scrollStyle: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#272822',
    marginTop: 30,
    borderRadius: 5
  },
  span: {
    fontSize: 14,
    backgroundColor: '#272822',
    fontFamily: 'Courier',
    padding: 15,
    flex: 1,
    color: 'white'
  }
});
