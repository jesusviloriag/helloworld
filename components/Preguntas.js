import React, { Component } from 'react';
import { Keyboard, FlatList, TextInput, StyleSheet, Text, View, TouchableOpacity , Platform, Image, Dimensions} from 'react-native';

import Respuesta from './Respuesta';

import { 
  AdMobBanner, 
  AdMobInterstitial
} from 'react-native-admob'

import { getPopular, getBuscar } from '../utils/fetcher';

export default class Preguntas extends Component {

  constructor(props) {
    super(props);

    var showAds = true;

    var idquestionAd2 = '';
      if (Platform.OS == "ios")
        idquestionAd2 = 'ca-app-pub-2466516236387107/3755796111';
      else
        idquestionAd2 = 'ca-app-pub-2466516236387107/3090657107';

    this.state = {
      mensaje: 'Hello, World!',
      numero: 25,
      placeHolder: 'Search language',
      text: '',
      paginaWeb: 'https://google.com',
      listaDeCodigosFuente: [],
      refreshing: false,
      language: props.language,
      idquestionAd2: idquestionAd2,
      showAds: showAds
    }

    if(showAds) {
      var idquestionAd = '';
      if (Platform.OS == "ios")
        idquestionAd = 'ca-app-pub-2466516236387107/5056451250';
      else
        idquestionAd = 'ca-app-pub-2466516236387107/6709326159';

      AdMobInterstitial.setAdUnitID(idquestionAd);
      AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
      AdMobInterstitial.requestAd().then(() => AdMobInterstitial.showAd());
    }

  }

  getBanner() {
    if(this.state.showAds) {
      return(
        <AdMobBanner
            adSize="bigBanner"
            adUnitID={this.state.idquestionAd2}
            testDevices={[AdMobBanner.simulatorId]}
            onAdFailedToLoad={error => console.error(error)}
          />
      )
    } else {
      return null;
    }
    
  }

  componentDidMount() {
    this.getCodigos();
  }

  getCodigos() {
    this.setState({ refreshing: true });
    var _this = this;
    getPopular(this.state.language, 1, 30)
      .then(function(response){
        console.log(response);
        _this.setState({listaDeRespuestas: response, refreshing: false})
      })
      .catch((error) => {
        console.error(error);
      });
  }

  buscar() {
    this.setState({ refreshing: true });
    var _this = this;
    _this.setState({listaDeRespuestas: []})
    getBuscar(this.state.query, this.state.language, 1, 30)
      .then(function(response){
        console.log(response);
        _this.setState({listaDeRespuestas: response, refreshing: false})
      })
      .catch((error) => {
        console.error(error);
      });
  }

  render() {
    return (

      <View style={styles.container}>

        <View style={{flexDirection: 'row'}}>
          <TextInput style={ styles.searchBox }
              onChangeText={text => this.setState({query: text})}
              placeholder="Search"
              autoCorrect={false}
              onSubmitEditing={this.buscar.bind(this)}
          />
          <TouchableOpacity style={{
              width: 40,
              height: 40,
              backgroundColor: '#0044aa',
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={this.buscar.bind(this)}
          >
            <Image
              style={{ width: 25, height: 25 }}
              resizeMode={"contain"}
              source={require('../assets/search.png')}
            />
          </TouchableOpacity>
        </View>
        

        <FlatList
          data={this.state.listaDeRespuestas}
          renderItem={({ item }) => <Respuesta nombre={item.titulo} link={item.link} />}
          onRefresh={this.getCodigos.bind(this)}
          refreshing={this.state.refreshing}
        />

        {this.getBanner()}


      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 33,
    textAlign: 'center',
    marginTop: 35,
    marginBottom: 10,
    fontWeight: 'bold',
    color: '#0044aa'
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  searchBox: {
    width: Dimensions.get("screen").width - 40,
    height: 40,
    borderColor: 'black',
    borderWidth: 2,
    paddingLeft: 10,
    backgroundColor: 'white'
  },
});
