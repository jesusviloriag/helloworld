import React, { Component } from 'react';
import { FlatList, TextInput, StyleSheet, Text, View, TouchableOpacity , Platform} from 'react-native';

import Codigo from './Codigo';

import { 
  AdMobBanner, 
  AdMobInterstitial
} from 'react-native-admob'

export default class App extends Component {

  constructor(props) {
    super(props);

    var idquestionAd2 = '';
    if (Platform.OS == "ios")
      idquestionAd2 = 'ca-app-pub-2466516236387107/3755796111';
    else
      idquestionAd2 = 'ca-app-pub-2466516236387107/1265364148';

    this.state = {
      mensaje: 'Hello, World!',
      numero: 25,
      placeHolder: 'Search language',
      text: '',
      paginaWeb: 'https://google.com',
      listaDeCodigosFuente: [],
      refreshing: false,
      idquestionAd2: idquestionAd2
    }

    console.log("DECOBEL");
    console.log(AdMobInterstitial);

    var idquestionAd = '';
    if (Platform.OS == "ios")
      idquestionAd = 'ca-app-pub-2466516236387107/5056451250';
    else
      idquestionAd = 'ca-app-pub-2466516236387107/6126808583';

    AdMobInterstitial.setAdUnitID(idquestionAd);
    AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
    AdMobInterstitial.requestAd().then(() => AdMobInterstitial.showAd());
  }

  componentDidMount() {
    this.getCodigos();
  }

  getCodigos() {
    this.setState({ refreshing: true });
    fetch("https://www.guaire.net/assets/helloworldapp/codigos.json")
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson);
        this.setState({ refreshing: false });
        this.setState({ listaDeCodigosFuente: responseJson.codigos });
      })
      .catch((error) => {
        console.error(error);
      });
  }

  imprimirCampoDeTexto() {
    return (
      <View style={{ flexDirection: 'row' }}>

        <TextInput
          style={{ backgroundColor: 'white', flex: 1, height: 50, borderColor: 'gray', borderWidth: 1 }}
          onChangeText={(text) => this.setState({ text })}
          value={this.state.text}
          placeholder={this.state.placeHolder}
        />

        <TouchableOpacity style={{
          width: 50,
          height: 50,
          backgroundColor: '#272822',
          alignItems: 'center',
          justifyContent: 'center',
        }}
          onPress={this.accionBoton.bind(this)}
        >
          <Text style={{
            fontSize: 20,
            textAlign: 'center',
            color: 'white',
            fontWeight: 'bold'
          }}>GO</Text>
        </TouchableOpacity>
      </View>


    )

  }

  render() {
    return (

      <View style={styles.container}>

        <FlatList
          data={this.state.listaDeCodigosFuente}
          renderItem={({ item }) => <Codigo nombre={item.nombre} foto={item.foto} ejemplo={item.ejemplo} />}
          onRefresh={this.getCodigos.bind(this)}
          refreshing={this.state.refreshing}
        />

        <AdMobBanner
          adSize="fullBanner"
          adUnitID={this.state.idquestionAd2}
          testDevices={[AdMobBanner.simulatorId]}
          onAdFailedToLoad={error => console.error(error)}
        />


      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 33,
    textAlign: 'center',
    marginTop: 35,
    marginBottom: 10,
    fontWeight: 'bold',
    color: '#0044aa'
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
