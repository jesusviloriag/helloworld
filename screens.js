import { Navigation } from 'react-native-navigation';

import App from './components/App';
import Codigo from './components/Codigo';
import Respuesta from './components/Respuesta';
import Preguntas from './components/Preguntas';

console.disableYellowBox = true;

// register all screens of the app (including internal ones)
export function registerScreens() {
    Navigation.registerComponent('helloworld.App', () => App);
    Navigation.registerComponent('helloworld.Codigo', () => Codigo);
    Navigation.registerComponent('helloworld.Respuesta', () => Respuesta);
    Navigation.registerComponent('helloworld.Preguntas', () => Preguntas);
}
