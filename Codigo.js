import React, { Component } from 'react';
import { Share, Clipboard, ScrollView, WebView, Image, Dimensions, TextInput, Platform, StyleSheet, Text, View, TouchableOpacity } from 'react-native';

export default class Codigo extends Component {

  constructor(props) {
    super(props);

    this.state = {
    }
  }

  share() {
    Share.share({
      message: this.props.ejemplo,
      title: 'Hello, World! example for ' + this.props.nombre
    });
  }

  copiar() {
    Clipboard.setString(this.props.ejemplo)
  }

  render() {
    return (
      <View style={styles.background}>
        <View style={styles.containerSided}>
          <Image
            style={{ width: 50, height: 50 }}
            resizeMode={"contain"}
            source={{ uri: this.props.foto }}
          />
          <Text style={styles.nombre}>{this.props.nombre}</Text>
          <TouchableOpacity style={{
            width: 35,
            height: 35,
            padding: 15,
            borderRadius: 10,
            backgroundColor: '#0044aa',
            alignItems: 'center',
            justifyContent: 'center',
            marginRight: 10
          }}
            onPress={this.copiar.bind(this)}
          >
            <Image
              style={{ width: 25, height: 25 }}
              resizeMode={"contain"}
              source={require('./assets/copy.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity style={{
            width: 35,
            height: 35,
            padding: 15,
            borderRadius: 10,
            backgroundColor: '#0044aa',
            alignItems: 'center',
            justifyContent: 'center'
          }}
            onPress={this.share.bind(this)}
          >
            <Image
              style={{ width: 25, height: 25 }}
              resizeMode={"contain"}
              source={require('./assets/share.png')}
            />
          </TouchableOpacity>
        </View>

        <View style={styles.containerSided}>
          <ScrollView horizontal={true} style={styles.scrollStyle}>
            <Text style={styles.ejemplo}>{this.props.ejemplo}</Text>
          </ScrollView>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#95e8ff',
    width: Dimensions.get('window').width,
    padding: 25,
    borderBottomWidth: 2,
    borderBottomColor: 'white'
  },
  containerSided: {
    flexDirection: 'row'
  },
  nombre: {
    fontSize: 22,
    textAlign: 'left',
    margin: 10,
    color: '#272822',
    fontWeight: 'bold',
    flex: 1
  },
  ejemplo: {
    fontSize: 14,
    backgroundColor: '#272822',
    fontFamily: 'Courier',
    padding: 15,
    flex: 1,
    color: 'white'
  },
  scrollStyle: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#272822',
    marginTop: 30,
    borderRadius: 5
  }
});
