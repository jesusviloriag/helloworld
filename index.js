import { Navigation } from 'react-native-navigation';

import { registerScreens } from './screens';

registerScreens(); // this is where you register all of your app's screens

// start the app
Navigation.startSingleScreenApp({
  screen: {
    screen: 'helloworld.App', // unique ID registered with Navigation.registerScreen
    title: 'Hello, World!', // title of the screen as appears in the nav bar (optional)
    navigatorStyle: {
      topBarElevationShadowEnabled: false,
      noBorder: true,
      navBarNoBorder: true,
    },
    navigatorButtons: {} // override the nav buttons for the screen, see "Adding buttons to the navigator" below (optional)
  },
  appStyle: {
    navBarNoBorder: true,
    navBarTextColor: '#0044aa',
    navBarBackgroundColor: 'white',
    navBarButtonColor: '#0044aa',
    topBarElevationShadowEnabled: false,
    noBorder: true,
    navBarNoBorder: true,
    navBarTitleTextCentered: true,
  }
});
